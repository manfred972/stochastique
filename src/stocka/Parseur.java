package stocka;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 *Classe permettant de parser un fichier
 */
public class Parseur {

	private ArrayList<Station> stations;

	public Parseur() {
		stations = new ArrayList<Station>();
		try {
			lire();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Station> getStations() {
		return stations;
	}
	

	/**
	 * Ajoute les parametres passes en parametre dans la station 
	 * @param station 
	 * @param s1
	 * @param s2
	 */
	public void ajout_set(Station station, String s1, String s2) {
		int taille = s2.length();
		if (s2.charAt(taille - 1) == ',')
			s2 = s2.substring(0, taille - 1);
		switch (s1.substring(1, s1.length() - 1)) {
		case "number":
			station.setNumber(Integer.parseInt(s2));
			break;
		case "name":
			station.setName(s2.substring(1, taille - 2));
			break;
		case "address":
			station.setAdress(s2.substring(1, taille - 2));
			break;
		case "lat":
			station.setLat(Double.parseDouble(s2));
			break;
		case "lng":
			station.setLng(Double.parseDouble(s2));
			break;
		case "banking":
			station.setBanking(Boolean.parseBoolean(s2));
			break;
		case "bonus":
			station.setBonus(Boolean.parseBoolean(s2));
			break;
		case "status":
			station.setStatus(s2.substring(1, taille - 2));
			break;
		case "contract_name":
			station.setContract(s2.substring(1, taille - 2));
			break;
		case "bike_stands":
			station.setK(Integer.parseInt(s2));
			break;
		case "available_bike_stands":
			station.setABS(Integer.parseInt(s2));
			break;
		case "available_bikes":
			station.setX(Integer.parseInt(s2));
			break;
		case "last_update":
			station.setTS(Long.parseLong(s2));
			break;
		}
	}

	/**
	 * Lecture d'un fichier
	 * @throws FileNotFoundException
	 */
	public void lire() throws FileNotFoundException {
		URL url = getClass().getResource("velib.txt");
		Scanner file = new Scanner(new File(url.getPath()));
		int separator;
		while (file.hasNextLine()) {
			String line = file.nextLine().trim();
			separator = 0;
			if (line.equals("{")) {
				Station s = new Station();
				while (separator < 2) {
					line = file.nextLine().trim();
					if (line.contains("}"))
						separator++;
					else if (!line.contains("{")) {
						String[] sp = line.split(":");
						sp[0] = sp[0].trim();
						sp[1] = sp[1].trim();
						ajout_set(s, sp[0], sp[1]);
					}

				}
				stations.add(s);
			}
		}
		file.close();
	}
}
