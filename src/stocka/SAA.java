package stocka;

import java.util.Scanner;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class SAA {
	private Echantillon[] sac_echantillon;
	private Echantillon e_deReference;
	private Scenario best_scenario;

	/**
	 * Constructeur de la classe SAA
	 * 
	 * @param nb_echantillons
	 *            : taille du sac d'echantillons
	 * @param taille_echantillon
	 *            : nombre de scenarios dans un echantillon
	 * @param s_initial
	 *            : scenario de reference
	 */
	public SAA(int nb_echantillons, int taille_echantillon, Scenario s_initial) {
		initialisation(nb_echantillons, taille_echantillon, s_initial);

		Scenario[] sol_echantillon = etape1();

		etape2(sol_echantillon);

		new Scanner(System.in).nextLine();
		best_scenario = etape3(sol_echantillon);
	}

	/**
	 * Initialisation de l'echantillon de reference ainsi que du sac
	 * d'echantillon
	 * 
	 * @param nb_echantillons
	 *            : taille du sac d'echantillons
	 * @param taille_echantillon
	 *            : nombre de scenarios dans un echantillon
	 * @param s_initial
	 *            : scenario de reference
	 */
	public void initialisation(int nb_echantillons, int taille_echantillon, Scenario s_initial) {
		System.out.println("\n\t---- Initialisation du SAA ----\n");
		System.out.println("nb_echantillons = " + nb_echantillons + ", taille_echantillon = " + taille_echantillon);

		// generer M echantillons de taille N chacun
		sac_echantillon = new Echantillon[nb_echantillons];

		for (int m = 0; m < nb_echantillons; m++) {
			sac_echantillon[m] = new Echantillon(taille_echantillon, s_initial, true);
		}
	}

	/**
	 * Etape 1: pour chaque echantillon on resoud le probleme a 2 niveaux
	 * 
	 * @return un tableau de scenarios contenant les meilleurs scenarios pour
	 *         chacuns des echantillons
	 */
	public Scenario[] etape1() {
		System.out.println("\n\t---- Etape 1 ----\n");
		Scenario[] solution_par_echantillon = new Scenario[sac_echantillon.length];

		for (int i = 0; i < sac_echantillon.length; i++) {
			RecuitStochastique rs = new RecuitStochastique(sac_echantillon[i]);
			solution_par_echantillon[i] = RecuitStochastique.getBestScenario(rs.getEchantillon());
		}
		return solution_par_echantillon;
	}

	/**
	 * 
	 * Etape 2: calculer la moyenne de la fo sur l'ensemble des echantillons
	 * fo_moy = (1/nb_echantillons)*SUM(fo_best)
	 * 
	 * @param sol_echantillon
	 *            : tableau des meilleurs scenarios
	 * @return la moyenne des fonctions objectif des meilleurs scenarios de
	 *         chacuns des echantillons
	 */
	public double etape2(Scenario[] sol_echantillon) {
		System.out.println("\n\t---- Fonction Objective Moyen ----\n");

		double sol = 0;

		for (Scenario s : sol_echantillon)
			sol += s.fonction_objective();

		System.out.print("moyenne = " + sol + " / " + sol_echantillon.length + " = ");
		sol /= sol_echantillon.length;
		System.out.println(sol);

		return sol;
	}

	/**
	 * Etape 3: estimer la vraie valeur de fo pour chaque echantillon en
	 * utilisant la solution optimal de 1er niveau de l'etape 1: refaire le
	 * calcul de fo avec x_best pour chacun des Echanti fo_i(x_best) avec i
	 * appartenant a l'ensemble E des echantillons
	 * 
	 * 
	 * Etape 4: choisir le scenario ou fo est la meilleure : x_SAA = fo_SAA =
	 * min (fo_best(x_))
	 * 
	 * @param sol_opti_E1
	 * @return
	 */
	public Scenario etape3(Scenario[] sol_opti_E1) {
		// init l'echantillon avec la solution optimale
		System.out.println("\n\t---- Generaion de l'echantillon de reference ----\n");
		Scenario[] res = new Scenario[sol_opti_E1.length];
		new Scanner(System.in).nextLine();
		int i = 0, id_best = 0;

		for (Scenario s : sol_opti_E1) {
			// generer l'echantillon de reference de taille N', tel que N' >> N
			e_deReference = new Echantillon(sac_echantillon[0].getScenarios().length * 3, s, false);

			res[i] = RecuitStochastique.getBestScenario(e_deReference);
			if (res[i].fonction_objective() <= res[id_best].fonction_objective())
				id_best = i;
			i++;
			new Scanner(System.in).nextLine();
		}

		return sol_opti_E1[id_best];
	}

	public Scenario getS_best() {
		return best_scenario;
	}

}
