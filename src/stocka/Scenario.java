package stocka;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class Scenario implements Cloneable {
	private ArrayList<Station> stations;
	private double temperature;
	private double probabilite;


	/**
	 * Constructeur de la classe Scenario
	 * 
	 * @param temp
	 *            : temperature
	 * @param p
	 *            : probabilite que ce scenario survienne
	 * @param station
	 *            : liste ddes stations intervenant dans ce scenario
	 */
	public Scenario(double temp, double p, ArrayList<Station> station) {
		temperature = temp;
		probabilite = p;
		stations = new ArrayList<Station>();
		for (int i = 0; i < station.size(); i++)
			stations.add((Station) station.get(i).clone());
	}

	public Object clone() {
		Scenario s = null;
		try {
			s = (Scenario) super.clone();
		} catch (CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		s.stations = new ArrayList<Station>();
		for (Station ss : stations)
			s.stations.add((Station) ss.clone());
		return s;
	}

	/**
	 * Calcule la valeur de la fonction objectif pour un scenario donne
	 * 
	 * @return le resultat de la fonction objective
	 */
	public double fonction_objective() {
		int first = 0;
		double second = 0;
		for (Station s : stations) {
			first += s.getC() * s.getX();
			for (Station ss : stations) {
				if (s != ss)
					second += s.getV() * s.getImoins(ss.getNumber());
			}
			second += s.getW() * s.getOmoins();
		}
		second = second * probabilite;
		return first + second;

	}

	public int calcul_somme(int variable, char var, Station station) {
		int somme = 0;
		for (Station s : stations) {
			if (s != station) {
				switch (variable) {
				case 1:
					if (var == 'j')
						somme += station.getBeta(s.getNumber());
					if (var == 'i')
						somme += s.getBeta(station.getNumber());
					break;
				case 2:
					somme += station.getKsi(s.getNumber());
					break;
				case 3:
					somme += station.getImoins(s.getNumber());
					break;
				}
			}
		}
		return somme;
	}

	/**
	 * Fonction permettant de verifier si le scenario verifie bien les contraintes du sujet
	 * @return false si l'une des contraintes n'est pas verifiee, true sinon
	 */
	public boolean verification_contraintes() {
		for (Station s : stations) {
			if (s.getX() > s.getK())
				return false;
			
			if (s.getIplus() - calcul_somme(3, 'j', s) != s.getX() - calcul_somme(2, 'j', s))
				return false;
			
			if (s.getOplus() - s.getOmoins() != s.getK() - s.getX() + calcul_somme(1, 'j', s) - calcul_somme(1, 'i', s))
				return false;
			
			for (Station ss : stations) {
				if (s != ss) {
					if (s.getBeta(ss.getNumber()) != s.getKsi(ss.getNumber()) - s.getImoins(ss.getNumber()))
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * Met a jour les variables du modele, a savoir O+, O-, I+, I- et ABS (noubre d'emplacement libre) 
	 * pour les deux station spassees en parametre
	 * @param a : station a
	 * @param b : station b
	 */
	public void calcule_modele(Station a, Station b) {
		unpdateStation(a);
		unpdateStation(b);
		
    	for (Station s : stations)
    		s.setOmoins(s.getX() + calcul_somme(1,'i',s) - s.getK() - calcul_somme(1,'j',s));
	}

	
	public void unpdateStation(Station a){
		int resteA = a.getX();
    	for (Station s : stations) {
    		if (s != a) {
    			if (resteA - a.getKsi(s.getNumber()) <= 0) {
					a.setBeta(s.getNumber(), resteA);
					resteA = 0;
				}
				else {
					resteA -=  a.getKsi(s.getNumber());
					a.setBeta(s.getNumber(), a.getKsi(s.getNumber()));
				}
    			a.setImoins(s.getNumber(), a.getKsi(s.getNumber()) - a.getBeta(s.getNumber()));
    		}
    		
    	}
    	a.setABS(a.getK() - a.getX()); 
    	a.setIplus(a.getX() - calcul_somme(1,'j',a));
	}
	
	/**
	 * Met a jour les beta en fonction de la demande et du nombre de velo disponible dans chacunes des stations 
	 * du scenario
	 */
	public void calcul_beta_global(){
				
		for (Station s : stations){
			calcule_beta(s);
		}
		for (int i = 0; i < stations.size() - 1; i++)
			calcule_modele(stations.get(i), stations.get(i + 1));
	}
	
	/**
	 * Fonction mettant a jour les beta d'une station passee en parametre
	 * @param s : station sur laquelle on travaille
	 */
	public void calcule_beta(Station s){
		int rest = s.getX();
		int b = 0;

		for(int i = 0; i < stations.size(); i++){
			if(i != s.getNumber()){
				b = Math.min(s.getKsi(i), rest);
				s.setBeta(i, b);
				rest -= b;
			}
		}
		
	}
	
	/**
	 * Initialisation du modele: les ksi
	 */
	public void init_modele() {
		
		int taille = stations.size(), borne_libre = 0, tmp = 0;
		Random r = new Random();
		for (Station s : stations) {
    		for (Station ss : stations) {
    			if (s != ss)
    				s.setKsi(ss.getNumber(), 0);
    		}
    	}
    	for (Station s : stations) {
    		borne_libre = s.getABS();
    		while (borne_libre > 0) {
    			do {
    				tmp = r.nextInt(taille);
    			}while(stations.get(tmp) == s);
    			borne_libre--;
    			Station ss = stations.get(tmp); 
    			s.setKsi(ss.getNumber(), s.getKsi(ss.getNumber()) +1);
    		}
    	}
    	init();
	}
	
	 
    public void init() {
    	int reste = 0;
    	for (Station s : stations) {
    		reste = s.getX();
    		for (Station ss : stations) {
    			if (s != ss) {
    				if (reste - s.getKsi(ss.getNumber()) <= 0) {
    					s.setBeta(ss.getNumber(), reste);
    					reste = 0;
    				}
    				else {
    					reste -=  s.getKsi(ss.getNumber());
    					s.setBeta(ss.getNumber(), s.getKsi(ss.getNumber()));
    				}
    				s.setImoins(ss.getNumber(), s.getKsi(ss.getNumber()) - s.getBeta(ss.getNumber()));
    			}
    		}
    	}
    	for (Station s : stations) {
    		s.setIplus(s.getX() - calcul_somme(1,'j',s));
        	s.setOmoins(s.getX() + calcul_somme(1,'i',s) - s.getK() - calcul_somme(1,'j',s));
    	}
    }

	public double getProbabilite() {
		return probabilite;
	}

	public double getTemperature() {
		return temperature;
	}

	public ArrayList<Station> getStations() {
		return stations;
	}

	public void setProbabilite(double p) {
		probabilite = p;
	}

	public void setTemperature(double t) {
		temperature = t;
	}

	public void setStations(ArrayList<Station> s) {
		stations = s;
	}
	
	
	public void affichage(){
		for(Station s : stations){
			s.affichage();
		}
	}

}